package org.htw.project;
import org.apache.commons.dbcp2.BasicDataSource;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class DbConnectionTest {

    @Test
    public void DbConnectionTest(){

    final BasicDataSource ds = new BasicDataSource();
        ds.setUrl("jdbc:mariadb://localhost:3306/radio");
        ds.setUsername("root");
        ds.setPassword(" ");
        ds.setMinIdle(1);
        ds.setMaxIdle(2);
        ds.setMaxOpenPreparedStatements(100);
        try(Connection con=ds.getConnection())
        {
            //ignore
        }
        catch (SQLException throwables)
        {throwables.printStackTrace();}
    }
}
