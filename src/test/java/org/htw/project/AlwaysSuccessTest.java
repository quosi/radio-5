package org.htw.project;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;


import static org.junit.jupiter.api.Assertions.assertTrue;

public class AlwaysSuccessTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(AlwaysSuccessTest.class);

    @Test
    public void alwaysSuccess(){
        LOGGER.debug("calling alwaysSuccess()");
        assertTrue(true);

    }
}

