package edu.sb.radio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Radio5Application {

    public static void main(final String[] args) {
        SpringApplication.run(Radio5Application.class, args);
    }
}