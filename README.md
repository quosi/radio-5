# Radio 5

<h3> HTW IMI 05-2020 M1 Kurs Project 1 </h3>
<br>
Ziel dieser Übung ist der Umgang mit Technologien zum Erstellen einer Web 2.0 Applikation. Diese soll ein minimalistisches Web-Radio darstellen, und es erlauben sowohl ein Server-basiertes als auch ein Peer-to-Peer basiertes Radioprogramm abzuspielen. <br><br>
Das Datenmodell besteht dabei aus einem relationalen Datenbankschema, persistenten Entitäten und Relationen, sowie REST-Services welche Zugriff auf diese Entitäten über Prozessgrenzen hinweg gewähren. Nachfolgend werden diese Service-Methoden dann asynchron aus einer Web-Applikation aufgerufen welche aus JavaScript basierten Controllern und einer HTML/CSS basierten View besteht.

## Installieren der radio-base-model.jar

In IntelliJ:

Auf der rechten Seite die Maven-Sitebar öffnen.
Das  dunkele "m" klicken (Execute Maven Goal) und den unten angezeigten Befehl ausführen.

```
mvn install:install-file -Dfile=lib/radio-model-base.jar -DgroupId=edu.sb.radio  -DartifactId=radio-model -Dversion=1.0 -Dpackaging=jar
```